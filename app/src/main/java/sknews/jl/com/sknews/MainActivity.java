package sknews.jl.com.sknews;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import sknews.jl.com.sknews.adapters.TweetAdapter;
import sknews.jl.com.sknews.utils.ButteryProgressBar;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private final String tag = "#SangguniangKabataan";

    private Twitter twitterClient;
    private boolean isTwitterClientReady;
    private boolean isRefreshing;
    private List<Status> freshList;


    private ListView tweetsListView;
    private TextView tweetsEmptyView;
    private ButteryProgressBar progressBar;
    private TextView freshTextView, hotTextView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(tag);

        tweetsListView = (ListView) findViewById(R.id.tweetsListView);
        tweetsEmptyView = (TextView) findViewById(R.id.tweetsEmptyView);
        freshTextView = (TextView) findViewById(R.id.freshTextView);
        hotTextView = (TextView) findViewById(R.id.hotTextView);
        progressBar = (ButteryProgressBar) findViewById(R.id.progressBar);

        freshTextView.setOnClickListener(this);
        hotTextView.setOnClickListener(this);

        new SearchTask().execute(tag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            if (!isRefreshing)
                new SearchTask().execute(tag);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        freshTextView.setEnabled(false);
        hotTextView.setEnabled(false);
        isRefreshing = true;
    }

    private void hideLoading() {
        progressBar.setVisibility(View.GONE);
        freshTextView.setEnabled(true);
        hotTextView.setEnabled(true);
        isRefreshing = false;
    }


    private void setUpTwitter4j() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setUseSSL(true);
        cb.setApplicationOnlyAuthEnabled(true);
        cb.setOAuthConsumerKey(getString(R.string.key_twitter_api_key));
        cb.setOAuthConsumerSecret(getString(R.string.key_twitter_api_secret));

        TwitterFactory tf = new TwitterFactory(cb.build());
        twitterClient = tf.getInstance();
        try {
            twitterClient.getOAuth2Token();
            isTwitterClientReady = true;
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.freshTextView:
                freshTextView.setSelected(true);
                hotTextView.setSelected(false);
                showFresh();
                break;
            case R.id.hotTextView:
                freshTextView.setSelected(false);
                hotTextView.setSelected(true);
                showHot();
                break;
        }
    }

    private void showFresh() {
        sortDate(freshList);
        TweetAdapter adapter = new TweetAdapter(MainActivity.this, freshList);
        tweetsListView.setAdapter(adapter);
    }

    private void showHot() {
        sortRank(freshList);
        TweetAdapter adapter = new TweetAdapter(MainActivity.this, freshList);
        tweetsListView.setAdapter(adapter);

    }

    private class SearchTask extends AsyncTask<String, Void, List<Status>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected List<twitter4j.Status> doInBackground(String... strings) {

            if (!isTwitterClientReady)
                setUpTwitter4j();

            List<twitter4j.Status> statuses = null;

            String keyword = strings[0];
            Query query = new Query(keyword);
            query.setCount(100);

            try {
                QueryResult result = twitterClient.search(query);
                statuses = result.getTweets();
            } catch (TwitterException e) {
                e.printStackTrace();
            }

            return statuses;
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> statuses) {
            super.onPostExecute(statuses);
            freshTextView.setSelected(true);
            freshList = statuses;

            TweetAdapter adapter = new TweetAdapter(MainActivity.this, statuses);
            tweetsListView.setAdapter(adapter);

            if (statuses.size() == 0) {
                if (tweetsListView.getVisibility() == View.VISIBLE) {
                    tweetsListView.setVisibility(View.GONE);
                    tweetsEmptyView.setVisibility(View.VISIBLE);
                }
            } else {
                if (tweetsListView.getVisibility() == View.GONE) {
                    tweetsListView.setVisibility(View.VISIBLE);
                    tweetsEmptyView.setVisibility(View.GONE);
                }
            }

            hideLoading();
        }
    }

    private void sortRank(List<twitter4j.Status> statuses){

        Collections.sort(statuses, new Comparator<twitter4j.Status>() {
            @Override
            public int compare(twitter4j.Status status, twitter4j.Status status2) {
                long lhs = status.getFavoriteCount() + status.getFavoriteCount();
                long rhs = status2.getFavoriteCount() + status2.getRetweetCount();
                return lhs < rhs ? 1 : -1;
            }
        });
    }

    private void sortDate(List<twitter4j.Status> statuses) {
        Collections.sort(statuses, new Comparator<Status>() {
            @Override
            public int compare(Status lhs, Status rhs) {
                return rhs.getCreatedAt().compareTo(lhs.getCreatedAt());
            }
        });
    }


}
